import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  {
    path: 'account',
    loadChildren: () => import('./account/account.module').then( m => m.AccountPageModule)
  },
  {
    path: 'shopping-card',
    loadChildren: () => import('./shopping-card/shopping-card.module').then( m => m.ShoppingCardPageModule)
  },
  {
    path: 'greeting-card',
    loadChildren: () => import('./greeting-card/greeting-card.module').then( m => m.GreetingCardPageModule)
  },
  {
    path: 'gift-card',
    loadChildren: () => import('./gift-card/gift-card.module').then( m => m.GiftCardPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
