import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CardService } from '../../services/card.service';

@Component({
  selector: 'app-card-count',
  templateUrl: './card-count.component.html',
})
export class CardCountComponent implements OnInit, OnDestroy {
  count = 0;
  countSubscription: Subscription | undefined;
  constructor(private cardService: CardService) {}

  ngOnInit() {
    this.countSubscription = this.cardService
      .getCardItems()
      .subscribe((value) => {
        this.count = value?.length;
      });
  }

  ngOnDestroy() {
    this.countSubscription.unsubscribe();
  }
}
