import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TermsModalComponent } from '../terms-modal/terms-modal.component';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';

@Component({
  selector: 'app-gift-card-terms',
  templateUrl: './gift-card-terms.component.html',
})
export class GiftCardTermsComponent {
  @Input() item: any;
  @Input() name: any;
  constructor(
    private modalController: ModalController,
    private iab: InAppBrowser
  ) {}

  async openTerms() {
    if (this.item.type === 'INLINE') {
      const modal = await this.modalController.create({
        component: TermsModalComponent,
        componentProps: {
          data: {
            title: this.name,
            description: this.item.text,
          },
        },
      });
      return await modal.present();
    } else if (this.item.type === 'LINK') {
      this.iab.create(this.item.url);
    }
  }
}
