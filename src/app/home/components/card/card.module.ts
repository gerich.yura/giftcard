import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddToOrderComponent } from '../add-to-order/add-to-order.component';
import { CardItemComponent } from '../card-item/card-item.component';
import { GiftCardTermsComponent } from '../gift-card-terms/gift-card-terms.component';
import { GiftCardValuesComponent } from '../gift-card-values/gift-card-values.component';
import { IonicModule } from '@ionic/angular';
import { TermsModalComponent } from '../terms-modal/terms-modal.component';
import { ShoppingItemComponent } from '../shopping-item/shopping-item.component';

const exportedDeclarations = [
  AddToOrderComponent,
  CardItemComponent,
  GiftCardTermsComponent,
  GiftCardValuesComponent,
  ShoppingItemComponent,
];

@NgModule({
  declarations: [...exportedDeclarations, TermsModalComponent],
  imports: [IonicModule, CommonModule],
  exports: [...exportedDeclarations],
})
export class CardModule {}
