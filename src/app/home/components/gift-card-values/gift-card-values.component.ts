import { Component, Input, OnInit } from '@angular/core';
import { CardService } from '../../services/card.service';

@Component({
  selector: 'app-gift-card-values',
  templateUrl: './gift-card-values.component.html',
})
export class GiftCardValuesComponent implements OnInit {
  @Input() item: any;
  @Input() index: number;
  @Input() isOrder: boolean;
  @Input() currency: string;
  maxValueExceeded: boolean;
  minValueExceeded: boolean;

  constructor(private cardService: CardService) {}

  ngOnInit() {
    if (!this.item.amount) {
      this.item.amount = this.item.valueRestrictions?.minimum
        ? this.item.valueRestrictions.minimum
        : 0;
    }
  }

  updateAmountInOrder(item) {
    this.cardService.updateItem(item, this.index);
  }

  setCardAmount(event: any) {
    this.maxValueExceeded = false;
    this.minValueExceeded = false;
    this.item.disabled = false;
    const currentValue = event.target.value;
    const maxValue = this.item.valueRestrictions?.maximum;
    const minValue = this.item.valueRestrictions?.minimum
      ? this.item.valueRestrictions.minimum
      : 0;
    this.item.amount = +currentValue;
    if (this.isOrder) {
      this.updateAmountInOrder(this.item);
    }
    if (currentValue > maxValue) {
      this.maxValueExceeded = true;
      this.item.disabled = true;
      return;
    }
    if (currentValue < minValue) {
      this.minValueExceeded = true;
      this.item.disabled = true;
      return;
    }
  }

  setExclusivelyValue(currentValue: number) {
    this.item.amount = currentValue;
    if (this.isOrder) {
      this.updateAmountInOrder(this.item);
    }
  }
}
