import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-terms-modal',
  templateUrl: './terms-modal.component.html',
})
export class TermsModalComponent {
  @Input() data: any;
  constructor(private modalController: ModalController) {}

  async close() {
    await this.modalController.dismiss();
  }
}
