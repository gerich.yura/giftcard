import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
})
export class CardItemComponent implements OnInit {
  @Input() item: any;
  @Input() currency: string;
  constructor() {}

  ngOnInit() {}
}
