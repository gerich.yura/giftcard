import { Component, Input, OnInit } from '@angular/core';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { AlertController, ToastController } from '@ionic/angular';
import { ShareInterface } from 'src/app/core/interfaces/interfaces';

@Component({
  selector: 'app-social-share',
  templateUrl: './social-share.component.html',
})
export class SocialShareComponent implements OnInit {
  @Input() item: ShareInterface;
  constructor(
    private socialSharing: SocialSharing,
    public toastController: ToastController
  ) {}

  ngOnInit() {}

  shareTelegram() {
    this.socialSharing
      .canShareVia('telegram')
      .then(() => {
        this.socialSharing.shareVia(
          'telegram',
          this.item.title,
          this.item.text,
          this.item.files[0],
          this.item.url
        );
      })
      .catch((error) => {
        this.toastMessage('You cannot share via Telegram');
      });
  }

  shareMessanger() {
    this.socialSharing
      .canShareVia('messenger')
      .then(() => {
        this.socialSharing.shareVia(
          'messenger',
          this.item.title,
          this.item.text,
          this.item.files[0],
          this.item.url
        );
      })
      .catch((error) => {
        this.toastMessage('You cannot share via Facebook');
      });
  }

  shareSkype() {
    this.socialSharing
      .canShareVia('skype')
      .then(() => {
        this.socialSharing.shareVia('skype',
          this.item.title,
          this.item.text,
          this.item.files[0],
          this.item.url
        );
      })
      .catch((error) => {
        this.toastMessage('You cannot share via Skype');
      });
  }

  shareWhatsapp() {
    this.socialSharing
      .canShareVia('whatsapp')
      .then(() => {
        this.socialSharing.shareViaWhatsApp(
          this.item.title,
          this.item.files[0],
          this.item.url
        );
      })
      .catch((error) => {
        this.toastMessage('You cannot share via WhatsApp');
      });
  }

  shareTwitter() {
    this.socialSharing
      .canShareVia('twitter')
      .then(() => {
        this.socialSharing.shareViaTwitter(
          this.item.title,
          this.item.files[0],
          this.item.url
        );
      })
      .catch((error) => {
        this.toastMessage('You cannot share via Twitter');
      });
  }

  shareInstagram() {
    this.socialSharing
      .canShareVia('instagram')
      .then(() => {
        this.socialSharing.shareViaInstagram(this.item.title, this.item.url);
      })
      .catch((error) => {
        this.toastMessage('You cannot share via Instagram');
      });
  }

  async toastMessage(message: string) {
    const toast = await this.toastController.create({
      header: message,
      position: 'top',
      duration: 2000,
    });
    toast.present();
  }
}
