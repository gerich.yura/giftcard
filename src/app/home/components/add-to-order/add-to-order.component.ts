import { Component, Input } from '@angular/core';
import { CardService } from '../../services/card.service';

@Component({
  selector: 'app-add-to-order',
  templateUrl: './add-to-order.component.html',
})
export class AddToOrderComponent {
  @Input() item: any;
  @Input() disabled: boolean;
  constructor(private cardService: CardService) {}

  async addToCard() {
    await this.cardService.saveItem(this.item).then(() => {
      console.log('add to card');
    });
    this.item.amount = this.item.valueRestrictions?.minimum
      ? this.item.valueRestrictions.minimum
      : 0;
  }
}
