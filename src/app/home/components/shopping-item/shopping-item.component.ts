import { Component, Input, OnInit } from '@angular/core';
import { CardService } from '../../services/card.service';

@Component({
  selector: 'app-shopping-item',
  templateUrl: './shopping-item.component.html',
})
export class ShoppingItemComponent implements OnInit {
  @Input() item: any;
  @Input() index: number;
  @Input() currency: string;
  constructor(private cardService: CardService) {}

  ngOnInit() {}

  removeItem() {
    this.cardService.deleteItem(this.index);
  }
}
