import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GreetingCardPage } from './greeting-card.page';

const routes: Routes = [
  {
    path: '',
    component: GreetingCardPage
  },
  {
    path: 'customize',
    loadChildren: () => import('./customize/customize.module').then( m => m.CustomizePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GreetingCardPageRoutingModule {}
