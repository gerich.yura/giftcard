import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GreetingCardPageRoutingModule } from './greeting-card-routing.module';

import { GreetingCardPage } from './greeting-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GreetingCardPageRoutingModule
  ],
  declarations: [GreetingCardPage]
})
export class GreetingCardPageModule {}
