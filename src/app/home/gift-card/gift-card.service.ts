import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GiftCardService {
  constructor(private http: HttpClient) {}

  getGiftItems() {
    return this.http.get('/assets/data.json').pipe(
      map((response: any) => response),
      catchError((error) => throwError(error))
    );
  }
}
