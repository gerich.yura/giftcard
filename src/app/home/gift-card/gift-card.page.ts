import { Component, OnInit } from '@angular/core';
import { CardService } from '../services/card.service';
import { GiftCardService } from './gift-card.service';

@Component({
  selector: 'app-gift-card',
  templateUrl: './gift-card.page.html',
})
export class GiftCardPage implements OnInit {
  cards: any;
  currency: string;
  constructor(
    private giftCardService: GiftCardService,
    private cardService: CardService
  ) {}

  ngOnInit() {
    this.getGiftCardList();
  }

  getGiftCardList() {
    this.giftCardService.getGiftItems().subscribe(async (data) => {
      this.changeCardLocation(data.currency);
      this.cards = data;
    });
  }

  changeCardLocation(currency: string) {
    this.cardService.setAppCurrency(currency);
    this.currency = currency;
  }
}
