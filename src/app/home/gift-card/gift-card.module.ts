import { NgModule } from '@angular/core';

import { GiftCardPageRoutingModule } from './gift-card-routing.module';

import { GiftCardPage } from './gift-card.page';
import { CardModule } from '../components/card/card.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardCountComponent } from '../components/card-count/card-count.component';

@NgModule({
  imports: [SharedModule, GiftCardPageRoutingModule, CardModule],
  declarations: [GiftCardPage, CardCountComponent],
})
export class GiftCardPageModule {}
