import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import Validation from 'src/app/core/validators/match-fields.validator';
import { passwordPattern } from 'src/app/core/validators/patterns.validator';
import { SuccessModalComponent } from 'src/app/shared/components/success-modal/success-modal.component';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
})
export class ResetPasswordPage {
  passwordForm: FormGroup;
  hidePassword: boolean;
  hideConfirmPassword: boolean;
  hideCurrentPassword: boolean;
  constructor(private modalController: ModalController) {
    this.passwordForm = new FormGroup(
      {
        currentPassword: new FormControl(null, [
          Validators.required,
          Validators.pattern(passwordPattern),
        ]),
        password: new FormControl(null, [
          Validators.required,
          Validators.pattern(passwordPattern),
        ]),
        passwordConfirm: new FormControl(null, [Validators.required]),
      },
      {
        validators: [Validation.match('password', 'passwordConfirm')],
      }
    );
  }

  async resetPassword() {
    if (this.passwordForm.valid) {
      const modal = await this.modalController.create({
        component: SuccessModalComponent,
        componentProps: {
          data: {
            title: 'Password was successfuly changed',
            description: 'Now you can use this password',
            btnText: 'Close',
            redirectUrl: '/menu/account',
          },
        },
      });
      return await modal.present();
    }
  }
}
