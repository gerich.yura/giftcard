import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { SuccessModalComponent } from 'src/app/shared/components/success-modal/success-modal.component';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
})
export class UserPage implements OnInit {
  userForm: FormGroup;
  constructor(
    private userService: UserService,
    private modalController: ModalController
  ) {
    this.userForm = new FormGroup({
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, [Validators.required]),
      phone: new FormControl(null, [Validators.required]),
      country: new FormControl(null, [Validators.required]),
      province: new FormControl(null, [Validators.required]),
      city: new FormControl(null, [Validators.required]),
      street: new FormControl(null, [Validators.required]),
      address: new FormControl(null, [Validators.required]),
      postalCode: new FormControl(null, [Validators.required]),
    });
  }

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo() {
    this.userService.getUser().subscribe((response) => {
      this.userForm.patchValue(response);
    });
  }

  async updateUserInfo() {
    if (this.userForm.valid) {
      const modal = await this.modalController.create({
        component: SuccessModalComponent,
        componentProps: {
          data: {
            title: 'User data was change',
            description: 'Please review your information',
            btnText: 'Close',
          },
        },
      });
      return await modal.present();
    }
  }
}
