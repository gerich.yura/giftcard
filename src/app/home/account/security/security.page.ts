import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Credentials, NativeBiometric } from 'capacitor-native-biometric';
import { biometricKey } from 'src/app/core/interfaces/constants';
import { SuccessModalComponent } from 'src/app/shared/components/success-modal/success-modal.component';

@Component({
  selector: 'app-security',
  templateUrl: './security.page.html',
})
export class SecurityPage implements OnInit {
  isBiometricActivate: boolean;

  constructor(
    public alertController: AlertController,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.isBioCredentialsEnable();
  }

  isBioCredentialsEnable() {
    NativeBiometric.getCredentials({
      server: biometricKey,
    }).then(
      (credentials: Credentials) => {
        if (credentials) {
          this.isBiometricActivate = true;
        }
      },
      (error) => {
        console.log('fail error');
        console.log(error);
      }
    );
  }

  async enableBioCredentials() {
    const alert = await this.alertController.create({
      header: 'Enter your login and password',
      message: 'Use your biometric to sign in or confirm payment.',
      inputs: [
        {
          name: 'username',
          type: 'email',
          placeholder: 'Email',
        },
        {
          name: 'password',
          type: 'password',
          placeholder: 'Password',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          },
        },
        {
          text: 'Confirm',
          handler: (credentials) => {
            console.log(credentials);
            alert.dismiss();
            this.verifyBiometric(credentials);
          },
        },
      ],
    });
    await alert.present();
  }

  verifyBiometric(credentials: Credentials) {
    NativeBiometric.verifyIdentity({
      reason: 'For sign in or confirm payment',
      title: 'Log in',
    }).then(
      () => {
        this.confirmBioCredentials(credentials);
      },
      (error) => {
        console.log('not set cred error');
        console.log(error);
        // Failed to authenticate
      }
    );
  }

  confirmBioCredentials(credentials) {
    NativeBiometric.setCredentials({
      username: credentials.username,
      password: credentials.password,
      server: biometricKey,
    }).then(() => {
      this.isBiometricActivate = true;
      this.successEnableMessage('Your biometric was enable');
    });
  }

  async successEnableMessage(title: string) {
    const modal = await this.modalController.create({
      component: SuccessModalComponent,
      componentProps: {
        data: {
          title,
          btnText: 'Close',
        },
      },
    });
    return await modal.present();
  }

  deleteBioCredentials() {
    NativeBiometric.deleteCredentials({
      server: biometricKey,
    }).then(() => {
      this.isBiometricActivate = false;
      this.successEnableMessage('Your biometric was remove');
    });
  }
}
