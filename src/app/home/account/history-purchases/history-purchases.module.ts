import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoryPurchasesPageRoutingModule } from './history-purchases-routing.module';

import { HistoryPurchasesPage } from './history-purchases.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoryPurchasesPageRoutingModule
  ],
  declarations: [HistoryPurchasesPage]
})
export class HistoryPurchasesPageModule {}
