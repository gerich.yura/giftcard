import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoryPurchasesPage } from './history-purchases.page';

const routes: Routes = [
  {
    path: '',
    component: HistoryPurchasesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoryPurchasesPageRoutingModule {}
