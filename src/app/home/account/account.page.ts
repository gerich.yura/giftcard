import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@capacitor/storage';
import { AuthenticationService } from 'src/app/core/servicecs/authentication.service';
import { tokenKey } from 'src/app/core/interfaces/constants';
import { AvailableResult, NativeBiometric } from 'capacitor-native-biometric';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
})
export class AccountPage implements OnInit {
  isBiometricAvailable: any;
  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) {}

  ngOnInit() {
    NativeBiometric.isAvailable().then(
      (result: AvailableResult) => {
        console.log(result);
        this.isBiometricAvailable = result.isAvailable;
        // const isFaceId = result.biometryType == BiometryType.FACE_ID;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  async logout() {
    await Storage.remove({ key: tokenKey });
    await this.authService.loadToken();
    this.router.navigateByUrl('/login');
  }
}
