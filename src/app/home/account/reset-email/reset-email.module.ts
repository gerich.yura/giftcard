import { NgModule } from '@angular/core';

import { ResetEmailPageRoutingModule } from './reset-email-routing.module';

import { ResetEmailPage } from './reset-email.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [SharedModule, ResetEmailPageRoutingModule],
  declarations: [ResetEmailPage],
})
export class ResetEmailPageModule {}
