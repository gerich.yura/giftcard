import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModalConfirmationCode } from 'src/app/core/interfaces/interfaces';
import Validation from 'src/app/core/validators/match-fields.validator';
import {
  emailPattern,
  passwordPattern,
} from 'src/app/core/validators/patterns.validator';
import { SuccessModalComponent } from 'src/app/shared/components/success-modal/success-modal.component';

@Component({
  selector: 'app-reset-email',
  templateUrl: './reset-email.page.html',
})
export class ResetEmailPage {
  resetEmailForm: FormGroup;
  hidePassword: boolean;
  openModal: boolean;
  data: ModalConfirmationCode;
  constructor(
    private router: Router,
    private modalController: ModalController
  ) {
    this.resetEmailForm = new FormGroup(
      {
        password: new FormControl(null, [
          Validators.required,
          Validators.pattern(passwordPattern),
        ]),
        email: new FormControl(null, [
          Validators.required,
          Validators.pattern(emailPattern),
        ]),
        confirmEmail: new FormControl(null, [
          Validators.required,
          Validators.pattern(emailPattern),
        ]),
      },
      {
        validators: [Validation.match('email', 'confirmEmail')],
      }
    );
  }

  async codeEntered(event) {
    this.openModal = false;
    await this.modalController.dismiss();
    this.successMessage();
  }

  async successMessage() {
    const modal = await this.modalController.create({
      component: SuccessModalComponent,
      componentProps: {
        data: {
          title: 'Email was successfuly changed',
          btnText: 'Close',
        },
      },
    });
    return await modal.present();
  }

  resetEmail() {
    if (this.resetEmailForm.valid) {
      this.openModal = true;
      this.data = {
        title: 'Confirmation code',
        description:
          'Enter one time code sent on ' +
          this.resetEmailForm.get('email').value,
        type: 'resetEmail',
        btnText: 'Verify Code',
      };
    }
  }
}
