import { Injectable } from '@angular/core';
import { GetResult, Storage } from '@capacitor/storage';
import { BehaviorSubject } from 'rxjs';
import { cardCurrency, cardValue } from 'src/app/core/interfaces/constants';

@Injectable({
  providedIn: 'root',
})
export class CardService {
  cardItems = new BehaviorSubject<any>(null);
  updateTotal = new BehaviorSubject<number>(0);
  constructor() {
    Storage.get({ key: cardValue }).then((data: GetResult) => {
      this.cardItems.next(JSON.parse(data.value));
    });
  }

  /* get cards currency */
  async getAppCurrency() {
    return await (
      await Storage.get({ key: cardCurrency })
    ).value;
  }

  /* setup cards currency */
  async setAppCurrency(currency: string) {
    await Storage.set({
      key: cardCurrency,
      value: currency,
    });
  }

  /* calculate total shipping card */
  calculateTotal() {
    Storage.get({ key: cardValue }).then((items: GetResult) => {
      const savedItems = items.value ? JSON.parse(items.value) : [];
      const total = savedItems.reduce(
        (value: number, currentValue: any) => value + +currentValue.amount,
        0
      );
      this.updateTotal.next(total);
    });
  }

  /* get total value shipping card */
  getTotal() {
    return this.updateTotal.asObservable();
  }

  /* get items form shipping card */
  getCardItems() {
    return this.cardItems.asObservable();
  }

  /* save item in shipping card*/
  async saveItem(card: any) {
    let savedItems = [];
    await Storage.get({ key: cardValue }).then((cards: GetResult) => {
      const savedCard = cards.value ? JSON.parse(cards.value) : [];
      if (savedCard?.length) {
        savedItems = [...savedCard, card];
      } else {
        savedItems = [card];
      }
    });
    this.updateItems(savedItems);
  }

  /* delete item from shipping card*/
  async deleteItem(index: number) {
    await Storage.get({ key: cardValue }).then((items: GetResult) => {
      const savedItems = items.value ? JSON.parse(items.value) : [];
      if (savedItems.length) {
        savedItems.splice(index, 1);
      }
      this.calculateTotal();
      this.updateItems(savedItems);
    });
  }

  /* update item in shipping card*/
  async updateItem(item, index) {
    await Storage.get({ key: cardValue }).then((items: GetResult) => {
      const savedItems = items.value ? JSON.parse(items.value) : [];
      savedItems[index] = item;
      this.updateItems(savedItems);
    });
  }

  /* update items */
  async updateItems(items) {
    await Storage.set({
      key: cardValue,
      value: JSON.stringify(items),
    });
    this.cardItems.next(items);
    this.calculateTotal();
  }
}
