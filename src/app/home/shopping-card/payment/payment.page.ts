import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PickerController } from '@ionic/angular';
import { Subscription } from 'rxjs/internal/Subscription';
import { CardService } from '../../services/card.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
})
export class PaymentPage implements OnInit {
  paymentForm: FormGroup;
  minDate: any;
  maxDate: any;
  diamondCost = 50;
  cardInfo = {
    type: 'default',
    code: 'CVV/CVC',
  };
  totalSubscription: Subscription | undefined;

  constructor(
    private pickerController: PickerController,
    private cardService: CardService
  ) {
    this.paymentForm = new FormGroup({
      amount: new FormControl(null, [Validators.required]),
      cardNumber: new FormControl(null, [Validators.required]),
      cardDate: new FormControl(null, [Validators.required]),
      cardName: new FormControl(null, [Validators.required]),
      cardCvv: new FormControl(null, [Validators.required]),
      postalCode: new FormControl(null, [Validators.required]),
    });
  }

  ngOnInit() {
    this.paymentForm.get('cardNumber').valueChanges.subscribe((value) => {
      if (value) {
        const cardNumber = value.replace(/\s/g, '');
        if (/^(?:4[0-9]{12}(?:[0-9]{3})?)$/g.test(cardNumber)) {
          this.cardInfo.code = 'CVV';
          this.cardInfo.type = 'visa';
        } else if (/^(?:5[1-5][0-9]{14})$/g.test(cardNumber)) {
          this.cardInfo.code = 'CVC';
          this.cardInfo.type = 'master';
        } else if (/^(?:3[47][0-9]{13})$/g.test(cardNumber)) {
          this.cardInfo.code = 'CID';
          this.cardInfo.type = 'amex';
        } else {
          this.cardInfo.code = 'CVV/CVC';
          this.cardInfo.type = 'default';
        }
      }
    });

    this.minDate = new Date();
    this.maxDate = this.setMaxDate();

    this.totalSubscription = this.cardService.getTotal().subscribe((value) => {
      this.paymentForm.get('amount').setValue(value);
    });
  }

  async setCardExpireDate() {
    const picker = await this.pickerController.create({
      mode: 'ios',
      keyboardClose: true,
      buttons: [
        {
          text: 'Confirm',
          handler: (selected) => {
            const cardDate = selected.month.value + '/' + selected.year.value;
            this.paymentForm.get('cardDate').setValue(cardDate);
          },
        },
      ],
      columns: [
        {
          name: 'month',
          options: this.monthValues(),
        },
        {
          name: 'year',
          options: this.yearValues(),
        },
      ],
    });
    await picker.present();
  }

  monthValues() {
    const month = [];
    for (let i = 1; i <= 12; i++) {
      month.push({
        text: i,
        value: i,
      });
    }
    return month;
  }

  yearValues() {
    const years = [];
    const d = new Date();
    const year = d.getFullYear();
    const yearMax = d.getFullYear() + 20;
    for (let i = year; i <= yearMax; i++) {
      years.push({
        text: i.toString().substring(2, 4),
        value: i.toString().substring(2, 4),
      });
    }
    return years;
  }

  setMaxDate() {
    const d = new Date();
    const year = d.getFullYear();
    const day = d.getDate();
    const result = new Date(year + 10, 11, day);
    return result;
  }

  pay() {}
}
