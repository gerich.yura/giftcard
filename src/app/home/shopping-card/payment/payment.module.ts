import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentPageRoutingModule } from './payment-routing.module';

import { PaymentPage } from './payment.page';
import { NgxMaskModule } from 'ngx-mask';
import { InputMaskDirective } from 'src/app/shared/directives/input-mask.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentPageRoutingModule,
    ReactiveFormsModule,
    NgxMaskModule,
  ],
  declarations: [PaymentPage, InputMaskDirective],
})
export class PaymentPageModule {}
