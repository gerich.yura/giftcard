import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShoppingCardPageRoutingModule } from './shopping-card-routing.module';

import { ShoppingCardPage } from './shopping-card.page';
import { CardModule } from '../components/card/card.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardModule,
    ShoppingCardPageRoutingModule,
  ],
  declarations: [ShoppingCardPage],
})
export class ShoppingCardPageModule {}
