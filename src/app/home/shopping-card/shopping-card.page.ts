import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-shopping-card',
  templateUrl: './shopping-card.page.html',
})
export class ShoppingCardPage implements OnInit, OnDestroy {
  cards = [];
  itemsSubscription: Subscription | undefined;
  totalSubscription: Subscription | undefined;
  currency: Promise<string>;
  total = 0;
  tax = 0;
  constructor(private cardService: CardService) {
    this.cardService.calculateTotal();
  }

  ngOnInit() {
    this.currency = this.cardService.getAppCurrency();
    console.log(this.currency);
    this.itemsSubscription = this.cardService
      .getCardItems()
      .subscribe((items) => {
        this.cards = items;
      });

    this.totalSubscription = this.cardService.getTotal().subscribe((value) => {
      this.total = value;
    });
  }

  ngOnDestroy() {
    console.log('card destroy');
    this.itemsSubscription.unsubscribe();
    this.totalSubscription.unsubscribe();
  }
}
