import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import Validation from 'src/app/core/validators/match-fields.validator';
import {
  emailPattern,
  passwordPattern,
} from 'src/app/core/validators/patterns.validator';
import { SuccessModalComponent } from 'src/app/shared/components/success-modal/success-modal.component';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.page.html',
})
export class ResetPage implements OnInit {
  passwordForm: FormGroup;
  hidePassword: boolean;
  hideConfirmPassword: boolean;
  constructor(
    private route: ActivatedRoute,
    private modalController: ModalController
  ) {
    this.passwordForm = new FormGroup(
      {
        password: new FormControl(null, [
          Validators.required,
          Validators.pattern(passwordPattern),
        ]),
        passwordConfirm: new FormControl(null, [Validators.required]),
        email: new FormControl(null, [
          Validators.required,
          Validators.pattern(emailPattern),
        ]),
      },
      {
        validators: [Validation.match('password', 'passwordConfirm')],
      }
    );
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      if (params?.email) {
        this.passwordForm.get('email').setValue(params.email);
      }
    });
  }

  async resetPassword() {
    if (this.passwordForm.valid) {
      const modal = await this.modalController.create({
        component: SuccessModalComponent,
        componentProps: {
          data: {
            title: 'Password was successfuly reset',
            description: 'Please login with new credentials',
            btnText: 'Login',
            redirectUrl: '/login',
          },
        },
      });
      return await modal.present();
    }
  }
}
