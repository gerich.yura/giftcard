import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import Validation from 'src/app/core/validators/match-fields.validator';
import {
  emailPattern,
  passwordPattern,
} from 'src/app/core/validators/patterns.validator';
import { SuccessModalComponent } from 'src/app/shared/components/success-modal/success-modal.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
})
export class RegisterPage {
  registerForm: FormGroup;
  hidePassword: boolean;
  hideConfirmPassword: boolean;
  constructor(private modalController: ModalController) {
    this.registerForm = new FormGroup(
      {
        firstName: new FormControl(null, [Validators.required]),
        lastName: new FormControl(null, [Validators.required]),
        email: new FormControl(null, [
          Validators.required,
          Validators.pattern(emailPattern),
        ]),
        phone: new FormControl(null, [Validators.required]),
        country: new FormControl(null, [Validators.required]),
        province: new FormControl(null, [Validators.required]),
        city: new FormControl(null, [Validators.required]),
        street: new FormControl(null, [Validators.required]),
        address: new FormControl(null, [Validators.required]),
        postalCode: new FormControl(null, [Validators.required]),
        password: new FormControl(null, [
          Validators.required,
          Validators.pattern(passwordPattern),
        ]),
        passwordConfirm: new FormControl(null, [Validators.required]),
      },
      {
        validators: [Validation.match('password', 'passwordConfirm')],
      }
    );
  }

  async register() {
    if (this.registerForm.valid) {
      const modal = await this.modalController.create({
        component: SuccessModalComponent,
        componentProps: {
          data: {
            title: 'User was successfuly created',
            description:
              'Please verify your account from email confirmation link',
            btnText: 'Login',
            redirectUrl: '/login',
          },
        },
      });
      return await modal.present();
    }
  }
}
