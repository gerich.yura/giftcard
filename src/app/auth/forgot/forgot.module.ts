import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ForgotPageRoutingModule } from './forgot-routing.module';
import { ForgotPage } from './forgot.page';

@NgModule({
  imports: [
    ForgotPageRoutingModule,
    SharedModule
  ],
  declarations: [ForgotPage],
})
export class ForgotPageModule {}
