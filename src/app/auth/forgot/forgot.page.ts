import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModalConfirmationCode } from 'src/app/core/interfaces/interfaces';
import { emailPattern } from 'src/app/core/validators/patterns.validator';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.page.html',
})
export class ForgotPage implements OnInit {
  forgotForm: FormGroup;
  openModal: boolean;
  data: ModalConfirmationCode;
  constructor(
    private router: Router,
    private modalController: ModalController
  ) {
    this.forgotForm = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern(emailPattern),
      ]),
    });
  }

  ngOnInit() {}

  codeEntered(event) {
    const code = event;
    if (event === '1234') {
      this.openModal = false;
      this.modalController.dismiss();
      const navigationExtras: NavigationExtras = {
        queryParams: {
          email: this.forgotForm.get('email').value,
        },
      };
      this.router.navigate(['/reset'], navigationExtras);
    } else {
      this.data = {
        ...this.data,
        errors: {
          isInvalid: true,
        },
      };
    }
  }

  async confirmForgotPassword() {
    if (this.forgotForm.valid) {
      this.openModal = true;
      this.data = {
        title: 'Confirmation code',
        description:
          'Enter one time code sent on ' + this.forgotForm.get('email').value,
        type: 'resetPassword',
        btnText: 'Verify Code',
      };
    }
  }
}
