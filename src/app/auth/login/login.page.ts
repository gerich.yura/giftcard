import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  emailPattern,
  passwordPattern,
} from 'src/app/core/validators/patterns.validator';
import { Storage } from '@capacitor/storage';
import { AuthenticationService } from 'src/app/core/servicecs/authentication.service';
import { Router } from '@angular/router';
import {
  AvailableResult,
  BiometryType,
  Credentials,
  NativeBiometric,
} from 'capacitor-native-biometric';
import { AlertController, ToastController, Platform } from '@ionic/angular';
import {
  biometricDisabled,
  biometricKey,
  tokenKey,
} from 'src/app/core/interfaces/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  hidePassword = false;
  isBiometricAvailable: any;
  isBiometricActivate: boolean;
  constructor(
    private authService: AuthenticationService,
    private router: Router,
    public alertController: AlertController,
    public toastController: ToastController,
    public platform: Platform
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
        Validators.pattern(emailPattern),
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.pattern(passwordPattern),
      ]),
    });
  }

  ngOnInit() {
    this.isBiometricEnable();
  }

  verifyBiometric(credentials: Credentials) {
    NativeBiometric.verifyIdentity({
      reason: 'For easy log in',
      title: 'Log in',
    }).then(
      () => {
        this.loginRedirect(credentials);
      },
      (error) => {
        console.log('not set cred error');
        console.log(error);
        // Failed to authenticate
      }
    );
  }

  isBiometricEnable() {
    NativeBiometric.isAvailable().then(
      (result: AvailableResult) => {
        console.log(result);
        this.isBiometricAvailable = result.isAvailable;
        // const isFaceId = result.biometryType == BiometryType.FACE_ID;
        if (this.isBiometricAvailable) {
          NativeBiometric.getCredentials({
            server: biometricKey,
          }).then(
            (credentials: Credentials) => {
              console.log(credentials);
              this.isBiometricActivate = true;
              this.verifyBiometric(credentials);
            },
            (error) => {
              console.log('fail error');
              console.log(error);
            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  async login() {
    if (this.loginForm.valid) {
      const isBioEnable = await Storage.get({ key: biometricDisabled });
      if (this.isBiometricAvailable && !isBioEnable.value) {
        NativeBiometric.getCredentials({
          server: biometricKey,
        }).then(
          (result: Credentials) => {
            this.loginRedirect(this.loginForm.value);
          },
          (error) => {
            console.log('Biometric was never set');
            this.enableBiometricCredentials(this.loginForm.value);
          }
        );
      } else {
        this.loginRedirect(this.loginForm.value);
      }
    }
  }

  async loginRedirect(credentials: Credentials) {
    const cred = credentials.username + '_' + credentials.password;
    await Storage.set({ key: tokenKey, value: cred });
    await this.authService.loadToken();
    this.router.navigateByUrl('/menu');
  }

  async enableBiometricCredentials(credentials: Credentials) {
    const alert = await this.alertController.create({
      header: 'Enable biometric authentification ?',
      message:
        'Use your biometric to sign in or confirm payment. You can always set it up later in Account Setting',
      buttons: [
        {
          text: 'Not now',
          role: 'cancel',
          handler: async () => {
            await Storage.set({
              key: biometricDisabled,
              value: 'isBiometricDisabled',
            });
            this.toastMessage('Biometric Not Set');
            this.loginRedirect(credentials);
          },
        },
        {
          text: 'Enable',
          role: 'close',
          handler: () => {
            NativeBiometric.verifyIdentity({
              reason: 'For easy log in',
              title: 'Log in',
            }).then(
              () => {
                NativeBiometric.setCredentials({
                  username: credentials.username,
                  password: credentials.password,
                  server: biometricKey,
                })
                  .then(() => {
                    this.isBiometricActivate = true;
                    this.toastMessage('Biometric Enable');
                  })
                  .then(() => {
                    this.loginRedirect(credentials);
                  });
              },
              (error) => {
                console.log('not set cred error');
                console.log(error);
                // Failed to authenticate
              }
            );
          },
        },
      ],
    });
    await alert.present();
  }

  removeBiometricCredentials() {
    NativeBiometric.deleteCredentials({
      server: biometricKey,
    }).then();
    this.isBiometricActivate = false;
  }

  async toastMessage(message: string) {
    const toast = await this.toastController.create({
      header: message,
      position: 'top',
      duration: 2000,
    });
    toast.present();
  }
}
