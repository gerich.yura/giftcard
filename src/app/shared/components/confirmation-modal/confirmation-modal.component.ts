import {
  Component,
  EventEmitter,
  Input,
  Output,
  ChangeDetectionStrategy,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { ModalConfirmationCode } from 'src/app/core/interfaces/interfaces';
import { confirmCodePattern } from 'src/app/core/validators/patterns.validator';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmationModalComponent {
  @Input() data: ModalConfirmationCode;
  @Output() eventData = new EventEmitter<string>();
  @Output() closeModal = new EventEmitter<boolean>();
  confirmationForm: FormGroup;
  constructor(private modalController: ModalController) {
    this.confirmationForm = new FormGroup({
      code: new FormControl(null, [
        Validators.required,
        Validators.pattern(confirmCodePattern),
        Validators.minLength(4),
      ]),
    });
  }

  close() {
    this.modalController.dismiss();
    this.closeModal.emit(false);
  }

  sendConfirmationForm() {
    if (this.confirmationForm.valid) {
      this.eventData.emit(this.confirmationForm.get('code').value);
    }
  }
}
