import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModalData } from 'src/app/core/interfaces/interfaces';

@Component({
  selector: 'app-success-modal',
  templateUrl: './success-modal.component.html',
})
export class SuccessModalComponent {
  @Input() data: ModalData;
  constructor(
    private modalController: ModalController,
    private router: Router
  ) {}

  async close() {
    await this.modalController.dismiss();
    if (this.data?.redirectUrl) {
      this.router.navigateByUrl(this.data.redirectUrl);
    }
  }
}
