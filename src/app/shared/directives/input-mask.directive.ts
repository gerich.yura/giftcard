import { Directive, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { IConfig, MaskDirective, MaskService, config } from 'ngx-mask';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'ion-input[mask]',
  providers: [MaskService],
})
export class InputMaskDirective extends MaskDirective {
  constructor(
    @Inject(DOCUMENT) private ionDocument: any,
    private ionMaskService: MaskService,
    @Inject(config) protected configVal: IConfig
  ) {
    super(ionDocument, ionMaskService, configVal);
  }
}
