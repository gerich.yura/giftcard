import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationModalComponent } from './components/confirmation-modal/confirmation-modal.component';
import { ErrorModalComponent } from './components/error-modal/error-modal.component';
import { SuccessModalComponent } from './components/success-modal/success-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

const exportedDeclarations = [
  ConfirmationModalComponent,
  ErrorModalComponent,
  SuccessModalComponent
];
const exportedModules = [
  CommonModule,
  ReactiveFormsModule,
  IonicModule
];

@NgModule({
  declarations: [...exportedDeclarations],
  imports: [...exportedModules],
  exports: [...exportedDeclarations, ...exportedModules]
})
export class SharedModule { }
