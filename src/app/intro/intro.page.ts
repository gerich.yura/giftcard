import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { introKey } from '../core/guards/intro.guard';
import { Storage } from '@capacitor/storage';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
})
export class IntroPage implements OnInit {
  @ViewChild('slider', { static: true }) slidefromHtml: IonSlides;
  slideOptions = {
    initialSlide: 0,
    speed: 400,
  };
  activeSlideIndex = 0;
  constructor(private router: Router) {}

  ngOnInit() {}

  getIonSlideIndex() {
    this.slidefromHtml.getActiveIndex().then((indx) => {
      this.activeSlideIndex = indx;
    });
  }

  async skip() {
    await Storage.set({ key: introKey, value: 'true' });
    this.router.navigate(['/login']);
  }
}
