export interface ModalConfirmationCode {
  title: string;
  description: string;
  type: string;
  btnText?: string;
  errors?: CodeErrors;
}
export interface CodeErrors {
  isExpired?: boolean;
  isInvalid?: boolean;
}

export interface ModalData {
  title: string;
  description?: string;
  btnText?: string;
  redirectUrl?: string;
}

export interface ShareInterface {
  title: string;
  text: string;
  subject?: string;
  files?: string[];
  url?: string;
  chooserTitle?: string;
  iPadCoordinates?: string;
}
