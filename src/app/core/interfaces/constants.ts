import { Storage } from '@capacitor/storage';
export const tokenKey = 'auth-token';
export const cardValue = 'card-value';
export const biometricDisabled = 'is-biometric-disabled';
export const biometricKey = 'www.egift.card.com';
export const cardCurrency = 'app-currency';
export const APP_CURRENCY_DEFAULT = 'USD';
