import { Injectable } from '@angular/core';
import {
  AsyncValidator,
  AbstractControl,
  ValidationErrors,
} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class UniqueEmailValidator implements AsyncValidator {
  constructor(private httpClient: HttpClient) {}
  validate(
    ctrl: AbstractControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.httpClient
      .post(environment.endpoint + 'auth/checkEmail', { email: ctrl.value })
      .pipe(
        map((response: any) => (response === false) ? { uniqueEmail: true } : null),
        catchError(() => of({ uniqueUser: true }))
      );
  }
}
