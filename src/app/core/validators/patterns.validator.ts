export const emailPattern = '^[a-z\\d]+([\\.\\_\\-]?[a-z\\d]+)+@[a-z\\d]+(\\.[a-z]+)+$';

export const confirmCodePattern = '^[A-Z\\d]*$';

export const usernamePattern = '^(?![\\.\\-\\@\\[\\]\\(\\)])[A-Za-z\\d\\-\\_\\.\\@\\(\\)\\[\\]\\s]*$';

export const firstNamePattern = '^[A-Za-z\\s]*$';

export const lastNamePattern = '^(?![\\u0027\\-])[A-Za-z\\-\\u0027\\s]*$';

export const streetAddressPattern = '^(?![\\u0027\\-\\/\\&])[A-Za-z\\d\\-\\u0027\\.\\#\\&\\/\\s]*$';

export const unitNumberPattern = '^(?![\\-\\/])[A-Za-z\\d\\-\\/\\s]*$';

export const cityPattern = '^(?![\\-])[A-Za-z\\-\\s]*$';

export const provincePattern = '^[A-Z]*$';

export const numberPattern = '^(\\d*)$';

export const patternCodeCanada = '^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$';

export const phonePattern = '^(\\+\\d{1})?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$';

export const passwordPattern = '^(?=^[^\\s]{8,16}$)(?=.*\\d.*)(?=.*[a-z].*)(?=.*[A-Z].*)(?=.*[!@#$%^&*()_\\-+=]).*$';
