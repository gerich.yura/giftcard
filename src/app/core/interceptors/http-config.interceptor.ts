import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { from, Observable, throwError } from 'rxjs';
import { catchError, map, switchMap, take } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';
import { AuthenticationService } from '../servicecs/authentication.service';
import { tokenKey } from '../interfaces/constants';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(private authService: AuthenticationService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return from(this.authService.getToken()).pipe(
      switchMap((token) => {
        if (token && token.value) {
          request = request.clone({
            headers: request.headers.set('Authorization', token.value),
          });
        }
        if (!request.headers.has('Content-Type')) {
          request = request.clone({
            headers: request.headers.set('Content-Type', 'application/json'),
          });
        }
        return next.handle(request).pipe(
          map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
              const authToken = event.headers.get('Authorization');
              if (authToken) {
                from(Storage.set({ key: tokenKey, value: authToken }));
                this.authService.isUserAuthenticated(true);
              }
            }
            return event;
          }),
          catchError((error: HttpErrorResponse) => {
            console.error(error);
            if (error instanceof HttpErrorResponse) {
              if (error.status === 401) {
                console.log(
                  '%c UNAUTHORIZED /!\\ ',
                  'border: 1px red solid; color: red;'
                );
                Storage.remove({ key: tokenKey });
              }
            }
            return throwError(error);
          })
        );
      })
    );
  }
}
