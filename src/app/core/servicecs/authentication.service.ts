import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, of, throwError } from 'rxjs';
import { Storage } from '@capacitor/storage';
import { environment } from 'src/environments/environment';
import { tokenKey } from '../interfaces/constants';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  userProfile = new BehaviorSubject<any>(null);
  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    null
  );
  token = '';
  user: any;

  constructor(private http: HttpClient) {
    this.loadToken();
  }

  /* get user profile */
  getUserProfile() {
    return this.userProfile.asObservable();
  }

  /* save user profile */
  saveUserProfile(user: any) {
    this.user = user;
    this.userProfile.next(user);
  }

  /* save auth token */
  async loadToken() {
    const token = await Storage.get({ key: tokenKey });
    if (token && token.value) {
      this.token = token.value;
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }

  /* get auth token */
  async getToken() {
    return await Storage.get({ key: tokenKey });
  }

  isUserAuthenticated(isAuth: boolean) {
    this.isAuthenticated.next(isAuth);
  }

  /* login route */
  login(user: any) {
    // return this.http.post(environment.endpoint + 'auth/signin', user).pipe(
    //   map((response: any) => response),
    //   catchError((error) => throwError(error))
    // );
  }
}
