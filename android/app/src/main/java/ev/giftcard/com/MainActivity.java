package ev.giftcard.com;

import android.os.Bundle;

import com.getcapacitor.Plugin;

import com.getcapacitor.BridgeActivity;

import java.util.ArrayList;

import com.capacitorjs.plugins.storage.StoragePlugin;

import com.epicshaggy.biometric.NativeBiometric;

public class MainActivity extends BridgeActivity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    registerPlugin(StoragePlugin.class);
    registerPlugin(NativeBiometric.class);

  }
}
